# Carmudi Exam PHP API Server by R.Laig

## Setup
 - Install using _composer install_
 - Configure _/config/prod.php_ for DB
 - Use MySQL, Create DB _carmudi_exam_db_ then run SQL dump with _/sql/carmudi_exam_db.sql_

## API Endpoints
 - *Fetch all records* GET - _/vehicles_
 - *Fetch single record* GET - _/vehicles/{id}_
 - *Add a record* POST - _/vehicles_
 - *Update a record* PUT - _/vehicles/{id}_
 - *Delete a record* DELETE - _/vehicles/{id}_

## Unit Testing
 - Configure _./tests/Services/VehiclesServiceTest.php_ for default testing DB (default DB: _carmudi_exam_test_db_)
 - Run _./vendor/bin/phpunit_