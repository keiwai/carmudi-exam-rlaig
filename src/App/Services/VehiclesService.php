<?php

namespace App\Services;

class VehiclesService extends BaseService
{

    public function getOne($id)
    {
        return $this->db->fetchAssoc("SELECT * FROM vehicles WHERE id=?", [(int) $id]);
    }

    public function getAll()
    {
        return $this->db->fetchAll("SELECT * FROM vehicles");
    }

    function save($vehicle)
    {
        $this->db->insert("vehicles", $vehicle);
        return $this->db->lastInsertId();
    }

    function update($id, $vehicle)
    {
        return $this->db->update('vehicles', $vehicle, ['id' => $id]);
    }

    function delete($id)
    {
        return $this->db->delete("vehicles", array("id" => $id));
    }

}
