<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class VehiclesController
{

    protected $vehiclesService;

    public function __construct($service)
    {
        $this->vehiclesService = $service;
    }

    public function getOne($id)
    {
        return new JsonResponse($this->vehiclesService->getOne($id));
    }

    public function getAll()
    {
        return new JsonResponse($this->vehiclesService->getAll());
    }

    public function save(Request $request)
    {
        $vehicle = $this->getDataFromRequest($request);
        return new JsonResponse(array("id" => $this->vehiclesService->save($vehicle)));
    }

    public function update($id, Request $request)
    {
        $vehicle = $this->getDataFromRequest($request);
        $this->vehiclesService->update($id, $vehicle);
        return new JsonResponse($vehicle);
    }

    public function delete($id)
    {
        return new JsonResponse($this->vehiclesService->delete($id));
    }

    public function getDataFromRequest(Request $request)
    {
        return $vehicle = array(
            "brand" => $request->request->get("brand"),
            "model" => $request->request->get("model"),
            "transmission" => $request->request->get("transmission"),
            "engine_power" => $request->request->get("engine_power"),
            "displacement_value" => $request->request->get("displacement_value"),
            "displacement_base_unit" => $request->request->get("displacement_base_unit"),
            "price" => $request->request->get("price"),
            "location" => $request->request->get("location")
        );
    }
}
