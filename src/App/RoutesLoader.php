<?php

namespace App;

use Silex\Application;

class RoutesLoader
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();

    }

    private function instantiateControllers()
    {
        $this->app['vehicles.controller'] = function() {
            return new Controllers\VehiclesController($this->app['vehicles.service']);
        };
    }

    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];

        $api->get('/vehicles', "vehicles.controller:getAll");
        $api->get('/vehicles/{id}', "vehicles.controller:getOne");
        $api->post('/vehicles', "vehicles.controller:save");
        $api->put('/vehicles/{id}', "vehicles.controller:update");
        $api->delete('/vehicles/{id}', "vehicles.controller:delete");

        $this->app->mount($this->app["api.endpoint"], $api);
    }
}

