<?php

namespace Tests\Services;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use App\Services\VehiclesService;


class VehiclesServiceTest extends \PHPUnit_Framework_TestCase
{

    private $vehicleService;

    public function setUp()
    {
        $app = new Application();
        $app->register(new DoctrineServiceProvider(), array(
            "db.options" => array(
             "driver" => "pdo_mysql",
             "user" => "root",
             "password" => "",
             "dbname" => "carmudi_exam_test_db",
             "host" => "localhost"
            ),
        ));
        $this->vehicleService = new VehiclesService($app["db"]);

        $stmt = $app["db"]->prepare("DROP TABLE IF EXISTS `vehicles`;");
        $stmt->execute();

        $stmt = $app["db"]->prepare("CREATE TABLE `vehicles` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `brand` varchar(255) NOT NULL DEFAULT '',
          `model` varchar(255) NOT NULL DEFAULT '',
          `transmission` varchar(255) NOT NULL DEFAULT '',
          `engine_power` varchar(255) NOT NULL DEFAULT '',
          `displacement_value` float NOT NULL DEFAULT '0',
          `displacement_base_unit` varchar(255) NOT NULL DEFAULT '' COMMENT 'L, CID or CC',
          `price` int(11) NOT NULL DEFAULT '0',
          `location` varchar(255) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
        )");
        $stmt->execute();
        
        $stmt = $app["db"]->prepare("INSERT INTO vehicles VALUES ('1','brand','model','transmission','0HP','0','L','0','location')");
        $stmt->execute();
    }

    public function testGetOne()
    {
        $data = $this->vehicleService->getOne(1);
        $this->assertEquals('brand', $data['brand']);
        $this->assertEquals('model', $data['model']);
        $this->assertEquals('transmission', $data['transmission']);
        $this->assertEquals('0HP', $data['engine_power']);
        $this->assertEquals('0', $data['displacement_value']);
        $this->assertEquals('L', $data['displacement_base_unit']);
        $this->assertEquals('0', $data['price']);
        $this->assertEquals('location', $data['location']);
    }

    public function testGetAll()
    {
        $data = $this->vehicleService->getAll();
        $this->assertNotNull($data);
    }

    function testSave()
    {
        $vehicle = array(
            "brand" => "brand1",
            "model" => "model1",
            "transmission" => "transmission1",
            "engine_power" => "1HP",
            "displacement_value" => "1",
            "displacement_base_unit" => "CC",
            "price" => "1",
            "location" => "location1"
        );
        $data = $this->vehicleService->save($vehicle);

        $data = $this->vehicleService->getAll();
        $this->assertEquals(2, count($data));
    }

    function testUpdate()
    {
        $vehicle = array(
            "brand" => "brand2",
            "model" => "model2",
            "transmission" => "transmission2",
            "engine_power" => "2HP",
            "displacement_value" => "2",
            "displacement_base_unit" => "CID",
            "price" => "2",
            "location" => "location2"
        );
        $this->vehicleService->save($vehicle);

        $vehicle = array(
            "brand" => "brand3",
            "model" => "model3",
            "transmission" => "transmission3",
            "engine_power" => "3HP",
            "displacement_value" => "3",
            "displacement_base_unit" => "CID",
            "price" => "3",
            "location" => "location3"
        );
        $this->vehicleService->update(1, $vehicle);

        $data = $this->vehicleService->getAll();
        $this->assertEquals("brand3", $data[0]["brand"]);

    }

    function testDelete()
    {
        $vehicle = array(
            "brand" => "brand2",
            "model" => "model2",
            "transmission" => "transmission2",
            "engine_power" => "2HP",
            "displacement_value" => "2",
            "displacement_base_unit" => "CID",
            "price" => "2",
            "location" => "location2"
        );
        $this->vehicleService->save($vehicle);
        $this->vehicleService->delete(1);

        $data = $this->vehicleService->getAll();
        $this->assertEquals(1, count($data));
    }

}
