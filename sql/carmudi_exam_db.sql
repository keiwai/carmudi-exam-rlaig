/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : carmudi_exam_db

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-06-24 08:35:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) NOT NULL DEFAULT '',
  `model` varchar(255) NOT NULL DEFAULT '',
  `transmission` varchar(255) NOT NULL DEFAULT '',
  `engine_power` varchar(255) NOT NULL DEFAULT '',
  `displacement_value` float NOT NULL DEFAULT '0',
  `displacement_base_unit` varchar(255) NOT NULL DEFAULT '' COMMENT 'L, CID or CC',
  `price` int(11) NOT NULL DEFAULT '0',
  `location` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('1', 'Mitsubishi', 'Strada', 'MT', '1000', '3.2', 'L', '900000', 'Makati');
INSERT INTO `vehicles` VALUES ('2', 'Honda', 'Fit', 'AT', '1000', '1.5', 'L', '300200', 'Alabang');
INSERT INTO `vehicles` VALUES ('3', 'Mitsubishi', 'Lancer', 'AT', '1000', '2', 'L', '500000', 'Makati');
