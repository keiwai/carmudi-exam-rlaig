<?php
require __DIR__ . '/prod.php';
$app['debug'] = true;
$app['log.level'] = Monolog\Logger::DEBUG;

$app['db.options'] = array(
 "driver" => "pdo_mysql",
 "user" => "root",
 "password" => "root",
 "dbname" => "carmudi_exam_db",
 "host" => "127.0.0.1",
);
