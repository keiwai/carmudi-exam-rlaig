<?php
$app['log.level'] = Monolog\Logger::ERROR;
$app['api.version'] = "v1";
$app['api.endpoint'] = "/api";

/** MySQL */
$app['db.options'] = array(
 "driver" => "pdo_mysql",
 "user" => "root",
 "password" => "",
 "dbname" => "carmudi_exam_db",
 "host" => "localhost",
);
